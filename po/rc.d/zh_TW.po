# Chinese (Traditional) translation for ltsp
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ltsp package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ltsp\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2010-11-10 10:25-0500\n"
"PO-Revision-Date: 2013-11-13 05:30+0000\n"
"Last-Translator: Chao-Hsiung Liao <j_h_liau@yahoo.com.tw>\n"
"Language-Team: Chinese (Traditional) <zh_TW@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-11-14 04:31+0000\n"
"X-Generator: Launchpad (build 16820)\n"

#: ../../rc.d/S15-userLoginCheck:34 ../../rc.d/S99-ltsp-cluster:12
#, sh-format
msgid ""
"The system thinks you are logged in elsewhere.  Would you like to close the "
"other session and continue to log in?"
msgstr "系統認為您已經從其他地方登入。您想要關閉其他的作業階段然後繼續登入嗎？"

#: ../../rc.d/I01-halt-check:9
#, sh-format
msgid "This system will halt in 10s."
msgstr "這個系統會在 10 秒內關機。"

#: ../../rc.d/S20-restrictUser:21
#, sh-format
msgid "User ${LDM_USERNAME} is not allowed to log into this workstation."
msgstr "使用者 ${LDM_USERNAME} 不被允許登入這個工作站。"

#: ../../rc.d/I01-nbd-checkupdate:36
#, sh-format
msgid "A new version of the system is available, rebooting in 10s."
msgstr "有新版本的系統可用，於 10 秒內重新開機。"
